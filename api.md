## 火烈鸟api

### 外链列表
    接口：api/api/getlink
    参数：
            -
    返回：
            [
                {
                    "id":1,
                    "linkname":"百度",
                    "link":"https:\/\/www.baidu.com",
                    "updatetime":1600327844,
                    "addtime":0},
                {
                    "id":2,
                    "linkname":"搜狐",
                    "link":"https:\/\/www.sohu.com",
                    "updatetime":1600327863,
                    "addtime":0
                }
            ]


### 会员福利 （商城部分）
    接口：api/api/getgoods
    参数：
             cates：   商品类别id
             area：    商品所在区域
    返回：
            {
                "total":2,
                "per_page":10,
                "current_page":1,
                "last_page":1, 
                "data":[
                    {
                        "id":3,
                        "goodsname":"戴尔DELL灵越5000 14英寸酷睿i5网课学习轻薄笔记本电脑(十代i5-1035G1 8G 512G MX230 2G独显)银",
                        "subtitle":"aaa",       // 商品简介
                        "upload_pic":"https:\/\/img.100cms.com.cn\/images\/72\/2020\/06\/jipv8X5I88wIRW9sP4AM4VZSR8wPMv5C.jpg",
                                                // 商品图片
                        "price_cur":"3999.00",  // 销售价
                        "price_del":"0.00",     // 划线价
                        "showsalesvolume":0,    // 是否显示销量
                        "salesvolume":0,        // 虚拟销量
                        "realsales":0,          // 实际销量
                        "goodsnum":0            // 商品库存
                    },
                    ……
                ]
            }

### 商品类别  
    接口：/api/api/getcates
    参数：
            ---
    返回：
        [
            {
                "id":1,
                "classname":"今日爆品、特价"
                },
            ……
        ]

### 会员购买商品放到购物车
    接口：/api/api/tocar
    参数：
           userid：      会员在用户表中的编号
           goodsid：     商品编号
           goodsname    商品名称
           pic_s        商品图片
           goodsprice   购买价格
           goodsnum     购买数量
    提交： post
    返回：
            {
                'code'： 1|0
            }

### 获取某人的购物车
    接口：/api/api/getordercar
    参数：
           userid：      会员在用户表中的编号    
    返回：
            [
                {
                    "id":25,
                    "shopid":0,
                    "userid":23,
                    "goodsid":3,
                    "goodsname":"戴尔DELL灵越5000 14英寸酷睿i5网课学习轻薄笔记本电脑(十代i5-1035G1 8G 512G MX230 2G独显)银",
                    "pic_s":"https:\/\/img.100cms.com.cn\/images\/72\/2020\/06\/jipv8X5I88wIRW9sP4AM4VZSR8wPMv5C.jpg",
                    "goodsprice":"3999.00",
                    "goodsnum":1,
                    "addtime":1598594537
                }
            ]       

### 购物车里删除商品
    接口：/api/api/delgoodsincar
    参数：
           id：商品在购物车表中的编号    
    返回：
            {
                'code': 1 // 删除成功
            }

### 根据用户 userid 获取订单
    接口：/api/api/getorderlistsbyuserid
    参数：
           userid 用户在会员表中的id编号    
    返回：
            [
                {
                    "id":61,    // 订单编号
                    "userid":33,
                    "orderstatus":0, // 订单状态
                    "ordertime":1599530942, // 下单时间
                    "paytime":0, // 支付时间
                    "paytype":1, // 支付类型，1 为微信
                    "goods":
                        {
                            "lists":[
                                {
                                    "id":84,
                                    "uniacid":0,
                                    "orderid":61,
                                    "goodsid":118,
                                    "goodspic":"https:\/\/img14.360buyimg.com\/n7\/jfs\/t1\/145143\/29\/1636\/222889\/5ef831dbE4ece7453\/5969340589cdabcb.jpg",
                                    "goodsname":"荣耀笔记本电脑MagicBook 14 14英寸全面屏轻薄本（AMD锐龙5 16G 512G 多屏协同 指纹Win10）银234",
                                    "price":"1.00",
                                    "total":1
                                }
                            ],
                            "money":1
                        },
                    }
                ]

### 支付成功后修改订单状态
    接口：api/api/modifyorder
    参数：
            orderid 订单表中的 id 号
    返回：
            {
                'code', 0，// 失败；1，成功
            }


### 用户注册
    接口：api/api/register
    参数：
            id            用户在数据库中的id
            username      用户名
            realname      真实姓名
            marrystatus   结婚状态
            sheng         省
            shi           市
            mobile        手机号
    返回：
            {
                'code', 0，// 失败；1，成功
                'msg'，    // 提示信息
            }

### 会员微信登录        
    接口：api/api/login
    参数：
            code        wx.login获取到的 code
            userinfo    用户授权后得到的用户信息
                        openId  
                        nickName
                        avatarUrl
                        gender
                        province
                        city
    返回：
            {
                'code': 1|0,
                'userid':
            }

## 充值后，修改用户的级别     
    接口：/api/api/modifyuserlevel
    参数：
            userid  用户的userid
            level   要修改成哪个等级
    返回：
            {
                "code":1,
                "userid":1
            }     

            1.充值的时间，
            2.到期时间

### 根据用户id生成二维码
    接口：/api/api/createqrcode
    参数：
            userid  自己的userid
    返回：
            如userid为空返回 0；
            否则返回二维码图片地址 path
            {
                "code":1,
                "path":"\/uploads\/qrcode\/40f4503947160aa3d7d721ca140f5027.jpg"
            }

### 首页 即图片4
    接口：api/api/home
    参数：
            gender         性别
            page           第几页
            userid         当前用户在用户表中的 id
    返回：
            {
                "total":6,
                "per_page":10,
                "current_page":1,
                "last_page":1,
                "data":[
                    {
                        "gender":0,
                        "pic_s":"http:\/\/s.img.mix.sina.com.cn\/auto\/crop?img=http%3A%2F%2Fn.sinaimg.cn%2Fent%2F4_img%2Fupload%2F9b7b89",
                        "uid":2,
                        "nickname":"带刺的玫瑰",
                        "age":18,
                        "height":"","
                        constellation":0,
                        "abstract":"",
                        "xindong":1, // 是否心动过
                        "zan":1 // 是否点赞过
                    },
                    {
                        "gender":0,
                        "pic_s":"http:\/\/s.img.mix.sina.com.cn\/auto\/crop?img=http%3A%2F%2Fn.sinaimg.cn%2Fent%2F4_img%2Fupload%2F89fcfc",
                        "uid":3,
                        "nickname":"涛声依旧",
                        "age":18,
                        "height":"",
                        "constellation":0,
                        "abstract":"",
                        "xindong":1,
                        "zan":1
                    },
                    ……
                ]
            }

### 对某人心动 | 取消对某人心动        
    接口：/api/api/xingdong
    参数：
            userid：对方的userid
            selfuserid：自己的userid
    返回：
            {
                "code":1,   // 1|0
                "msg":"",   // 空
                "id":"10"   // 不用管
            }            

### 心动我的列表
    接口：/api/api/xindongmelists
    参数：
            userid  自己的userid
    返回：
            {
                "total":2,
                "per_page":10,
                "current_page":1,
                "last_page":1,
                "data":[
                    {
                        "xindongid":23,
                        "addtime":1596876219,
                        "nickname":"刘秋成",
                        "avatarUrl":"http:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTLh0BLicHmiaSGnDjwvT9HPuIPwzv8xLAsbuU5dv6fdOGEEnIBUk1ib19FTg8KxzrkTZjHjFHiapanmww\/132"
                    },
                    {
                        "xindongid":2,
                        "addtime":1596876219,
                        "nickname":"带刺的玫瑰",
                        "avatarUrl":"https:\/\/gitee.com\/static\/images\/logo-black.svg?t=158106664"
                    }
                ]
            }

### 我的心动列表         
    接口：/api/api/mexindonglists
    参数：
            userid  自己的userid
    返回：   
            {
                "total":2,
                "per_page":10,
                "current_page":1,
                "last_page":1,
                "data":[
                    {
                        "userid":3,
                        "addtime":1596876219,
                        "nickname":"涛声依旧",
                        "avatarUrl":"https:\/\/dss0.bdstatic.com\/6Ox1bjeh1BF3odCf\/it\/u=3540234859,271493783&fm=85&app=92&f=JPEG?w=121&h=75&s=65FAA22B40AB6CBE0C08D0DB030010B4"
                    },
                    {
                        "userid":23\,
                        "addtime":0,
                        "nickname":"刘秋成",
                        "avatarUrl":"http:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTLh0BLicHmiaSGnDjwvT9HPuIPwzv8xLAsbuU5dv6fdOGEEnIBUk1ib19FTg8KxzrkTZjHjFHiapanmww\/132"
                    }
                ]
            }

### 对我点赞的
    接口：/api/api/zanmelists
    参数：
            $userid 自己的userid    
    返回：
            {
                "total":2,
                "per_page":10,
                "current_page":1,
                "last_page":1,
                "data":[
                    {
                        "zanid":2,
                        "addtime":1598973219,
                        "nickname":"带刺的玫瑰",
                        "avatarUrl":"https:\/\/gitee.com\/static\/images\/logo-black.svg?t=158106664"
                    },
                    {
                        "zanid":3,
                        "addtime":1598974219,
                        "nickname":"涛声依旧",
                        "avatarUrl":"https:\/\/dss0.bdstatic.com\/6Ox1bjeh1BF3odCf\/it\/u=3540234859,271493783&fm=85&app=92&f=JPEG?w=121&h=75&s=65FAA22B40AB6CBE0C08D0DB030010B4"
                    }
                ]
            } 

### 对某人点赞
    接口：/api/api/zan
    参数：
            userid:  对方的userid
            zanid：  自己的userid   
    返回：    
            {
                "code": 1|0
            }            

### 动态广场（全部）
    接口：/api/api/getdynamic
    参数：
            userid：当前浏览者在用户表中的 id
                    示例userid:23
            page
    返回：
        {
            "total":2,
            "per_page":10,
            "current_page":1,
            "last_page":1,"
            data":
            [
                {
                    "did":1, // 动态id
                    "userid":23,
                    "ispic":1, // 是否有图片
                    "content":"<ul class=\"itemlist list-item list-paddingleft-2\" style=\"list-style-type: none;\"><li><p><br\/><\/p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px;\">坏单包赔<\/p><p class=\"dec-footer\" style=\"margin-bottom: 0px; padding: 0px; color: rgb(102, 102, 102); font-size: 12px; font-family: 宋体; width: 640px; line-height: 15px;\">坏单包赔是淘宝汇吃推出的一项优质服务，主要包括破坏破损包赔和商品品质腐烂<\/p><\/li><\/ul><p style=\"margin-top: 1.12em; margin-bottom: 1.12em; padding: 0px;\"><br\/><\/p>",
                    "visited":25, // 访问次数
                    "post_city":"保定市",
                    "showaddress":1, // 是否显示动态发布时候的地址
                    "addtime":1596875219,
                    "avatarUrl":"http:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTLh0BLicHmiaSGnDjwvT9HPuIPwzv8xLAsbuU5dv6fdOGEEnIBUk1ib19FTg8KxzrkTZjHjFHiapanmww\/132",
                    "nickname":"刘秋成",
                    "level":1,
                    "age":18,
                    "sex":0,
                    "constellation":2,
                    "marrydate":0,
                    "levelname":"普通会员",
                    "pics":[ // 动态图片列表
                        {
                            "pic_s":"https:\/\/m1-1253159997.image.myqcloud.com\/imageDir\/5259ccc559f16769dc65768d58dcda2b.jpg"
                        },
                        {
                            "pic_s":"https:\/\/m1-1253159997.image.myqcloud.com\/imageDir\/d501adf00b10430add432f307dccda1d.jpg"
                        },
                        {
                            "pic_s":"https:\/\/f12.baidu.com\/it\/u=193925234,4239981038&fm=76"
                        }
                    ]
                },
            ]
        }

### 动态广场（心动）
    接口：/api/api/getdynamic_xindong
    参数：
            userid：当前浏览者在用户表中的 id 
                    示例userid:23
            page
    返回：
            {
                "total":2,
                "per_page":10,
                "current_page":1,
                "last_page":1,
                "data":[
                    {
                        "did":4,
                        "userid":3,
                        "ispic":1,
                        "content":"eeeee",
                        "visited":220,
                        "post_city":"保定市",
                        "showaddress":1,
                        "addtime":1598975219,
                        "avatarUrl":"",
                        "nickname":"涛声依旧",
                        "level":1,
                        "age":18,
                        "sex":0,
                        "constellation":0,
                        "marrydate":0,
                        "levelname":"普通会员",
                        "pics":[
                            {
                                "pic_s":"https:\/\/dss3.baidu.com\/-rVXeDTa2gU2pMbgoY3K\/it\/u=2872145868,3795099412&fm=202&mola=new&crop=v1"
                            },
                            {
                                "pic_s":
                                "https:\/\/dss0.bdstatic.com\/6Ox1bjeh1BF3odCf\/it\/u=1303017126,349562650&fm=85&app=92&f=JPEG?w=121&h=75&"
                            },
                            {
                                "pic_s":"https:\/\/dss0.bdstatic.com\/6Ox1bjeh1BF3odCf\/it\/u=2025121965,1524428413&fm=85&app=92&f=JPEG?w=121&h=75"
                            }
                        ]
                    }
                ]
            }

### 动态详情
    接口：/api/api/getdynamicshow
    参数：
            id：动态id
    返回：
            {
                "id":1,
                "userid":23,
                "ispic":1,
                "content":"<ul class=\"itemlist list-item list-paddingleft-2\" style=\"list-style-type: none;\"><li><p><br\/><\/p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px;\">坏单包赔<\/p><p class=\"dec-footer\" style=\"margin-bottom: 0px; padding: 0px; color: rgb(102, 102, 102); font-size: 12px; font-family: 宋体; width: 640px; line-height: 15px;\">坏单包赔是淘宝汇吃推出的一项优质服务，主要包括破坏破损包赔和商品品质腐烂<\/p><\/li><\/ul><p style=\"margin-top: 1.12em; margin-bottom: 1.12em; padding: 0px;\"><br\/><\/p>",
                "visited":25,
                "post_province":23,
                "post_city":"保定市",
                "post_area":4049,
                "showaddress":1,
                "status":1,
                "addtime":1596875219,
                "pics":[
                    {"pic_s":"https:\/\/m1-1253159997.image.myqcloud.com\/imageDir\/5259ccc559f16769dc65768d58dcda2b.jpg"},
                    {"pic_s":"https:\/\/m1-1253159997.image.myqcloud.com\/imageDir\/d501adf00b10430add432f307dccda1d.jpg"},
                    {"pic_s":"https:\/\/f12.baidu.com\/it\/u=193925234,4239981038&fm=76"}
                ],
                "userinfo":{
                    "nickname":"刘秋成",
                    "avatarUrl":"http:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTLh0BLicHmiaSGnDjwvT9HPuIPwzv8xLAsbuU5dv6fdOGEEnIBUk1ib19FTg8KxzrkTZjHjFHiapanmww\/132",
                    "age":18,
                    "sex":1,
                    "constellation":2,
                    "marrydate":0
                }
            }

### 对会员的动态点赞
    接口：/api/api/dynamicslike
    参数：
            dynamicsid： 会员动态的dynamicsid
            userid：     自己的userid
    返回：
            {
                "code": 1|0
            }

### 给会员动态添加评论     
    接口：/api/api/addpinglun
    参数：
            dynamicsid： 会员动态的 id
            userid：     评论人在用户表中的id
            content：    评论内容
    返回：
            {
                "code":1,
                "msg":"评论完成",
                "id":"1"
            }

### 保存会员发布的动态 
    接口：/api/api/savedynamics
    参数：
             userid         会员的 id
             ispic          是否有图片
             content        动态内容
             post_province  发布动态时所在的省份 河北
             post_city      发布动态时候所在的市 保定
             post_area      发布动态时所在的地区 涿州
             showaddress
    返回： 
            {
                "code":1,
                "msg":"发布成功",
                "id":"1"
            }

### 获取会员动态的评论列表     
    接口：/api/api/getdynamicslists
    参数：
            dynamicsid： 会员动态的 id
            userid       当前浏览者在用户表中的 id
    返回：
            {
                "total":1,
                "per_page":10,
                "current_page":1,
                "last_page":1,
                "data":[
                    {
                        "dynamicsid":3,
                        "userid":23,         // 评论人在用户表中的id
                        "content":"abcdefg", // 评论内容
                        "addtime":1598494726,
                        "nickname":"刘秋成",  // 评论人的妮称
                        "avatarUrl":"http:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTLh0BLicHmiaSGnDjwvT9HPuIPwzv8xLAsbuU5dv6fdOGEEnIBUk1ib19FTg8KxzrkTZjHjFHiapanmww\/132"
                    }
                ]
            }

### 获取某人的朋友圈        
    接口：/api/api/getfriendcircle
    参数：
            userid 在用户表中的id
    返回：
            {
                "userinfo":{
                    "id":23,
                    "nickname":"刘秋成",
                    "avatarUrl":"http:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTLh0BLicHmiaSGnDjwvT9HPuIPwzv8xLAsbuU5dv6fdOGEEnIBUk1ib19FTg8KxzrkTZjHjFHiapanmww\/132",
                    "age":18,
                    "sex":1,
                    "photo":"https:\/\/dss0.bdstatic.com\/6Ox1bjeh1BF3odCf\/it\/u=2246262073,2843724819&fm=74&app=80&f=JPEG&size=f121,90?sec=1880279984&t=21167d73f9472172af36141f72ab02a1",
                    "constellation":2,
                    "marrydate":0,
                    "abstract":""
                },
                "pics":[
                    {
                        "id":3,
                        "dynamicsid":1,
                        "userid":23,
                        "pic_s":"https:\/\/f12.baidu.com\/it\/u=193925234,4239981038&fm=76",
                        "addtime":1596364177
                    },
                    {
                        "id":2,
                        "dynamicsid":1,
                        "userid":23,
                        "pic_s":"https:\/\/m1-1253159997.image.myqcloud.com\/imageDir\/d501adf00b10430add432f307dccda1d.jpg","addtime":1596214177
                    }
                    ]
            }

### 获取某人的相册
    接口：/api/api/getphotos
    参数：
             userid 某人在用户表中的 id
             back：json      
    返回：
            [
                {
                    "id":3,
                    "dynamicsid":1,
                    "userid":23,
                    "pic_s":"https:\/\/f12.baidu.com\/it\/u=193925234,4239981038&fm=76",
                    "addtime":1596364177
                },
                {
                    "id":2,
                    "dynamicsid":1,
                    "userid":23,
                    "pic_s":"https:\/\/m1-1253159997.image.myqcloud.com\/imageDir\/d501adf00b10430add432f307dccda1d.jpg",
                    "addtime":1596214177
                },
                ……
            ]   

### 获取某人的择偶标准 图21
    接口：/api/api/getchoose
    参数：
            userid   某人在用户表中的 id
    返回：
            {
                "code": 1,
                "info": {
                    "id": 2,
                    "userid": 2,
                    "height": "172",
                    "oldyear": 45,
                    "education": 0,
                    "oldsheng": 23,
                    "oldshi": 4031,
                    "oldxian": 4049,
                    "salary": "1",
                    "marrystatus": 1, // 0，不限；1，未婚；2，离异；3，丧偶
                    "childs": "一儿一女",
                    "sheng": 23,      // 现居住省 id
                    "shi": 4031,
                    "xian": 4049,
                    "havecar": 0,
                    "havehouse": 0,
                    "updatetime": 0,
                    "addtime": 1595387013,
                    "sheng_name": "河北省", // 现居住省
                    "shi_name": "保定市",   // 现居住市
                    "xian_name": "涿州市"   // 现居住县
                }
            }

### 保存某人的择偶标准 图21
    接口：  /api/api/savechoose
    参数：
            userid
            height
            oldyear
            education
            oldsheng
            oldshi
            oldxian
            salary
            marrystatus
            childs
            sheng
            shi
            xian
            havecar
            havehouse
    返回：    
            {
                'code'：1|0
            }

### 获取某人的收货地址
    接口：/api/api/getuseraddress
    参数：
            userid   某人在用户表中的 id
    返回：
            [
                {
                    "id": 3,
                    "userid": 23,
                    "province": 23,
                    "city": 4031,
                    "area": 4049,
                    "address": "aa",
                    "user": "ddd",
                    "tel": "aaaa",
                    "isdefault": 1,
                    "updatetime": 0,
                    "addtime": 0
                },
                {
                    "id": 2,
                    "userid": 23,
                    "province": 23,
                    "city": 4031,
                    "area": 4049,
                    "address": "冠云东路300号",
                    "user": "李强",
                    "tel": "13600000005",
                    "isdefault": 0,
                    "updatetime": 0,
                    "addtime": 0
                }
            ]       

### 保存某人的收货地址
    接口：/api/api/saveuseraddress
    参数：
                userid
                province
                city
                area
                address
                user
                tel
    返回：
            {
                'code'  => 1,
                'msg'   => '保存完成！',
                'id'    => $result                
            }

### 设置某人的默认收货地址
    接口：/api/api/setdefaultuseraddress
    参数：
            userid      某人的在用户表中的 id
            addressid   某人收货地址的 id
    返回：
            {
                "code": 1|0
            }

### 删除某人的收货地址
    接口：/api/api/deluseraddress
    参数：
            id 认收货地址的 id
    返回：
            {
                "code": 1|0
            }    

### 获取某人的基本信息
    接口：/api/api/getuserbaseinfo
    参数：
            userid 某人的在用户表中的 id
    返回：
            {
                "photo":"https:\/\/dss0.bdstatic.com\/6Ox1bjeh1BF3odCf\/it\/u=2246262073,2843724819&fm=74&app=80&f=JPEG&size=f121,90?sec=1880279984&t=21167d73f9472172af36141f72ab02a1",
                "avatarUrl":"http:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTLh0BLicHmiaSGnDjwvT9HPuIPwzv8xLAsbuU5dv6fdOGEEnIBUk1ib19FTg8KxzrkTZjHjFHiapanmww\/132",
                "nickname":"刘秋成",
                "age":18,
                "pincodes":"",
                "realname":"",
                "sex":1,
                "sheng":0,
                "shi":0,
                "xian":0,
                "salary":"0",
                "marrydate":0
            }

### 设置某人的基本信息
    接口：/api/api/setuserbaseinfo
    参数：
            userid
            avatarUrl
            online
            nickname
            age
            sex
            height
            sheng
            shi
            xian
            constellation
            education
            salary
            marrydate
            marrystatus
            childs
    返回：
            {                
                'code'  => 1,
                'msg'   => '保存完成！'
            }

### 搜索会员
    接口：/api/api/search
    参数：            
            level
            heightmin   // 最小身高
            heightmax
            agemin      // 最小年龄
            agemax
            constellation// 星座
            salarymin   // 最低工资
            salarymax
            education   // 学历
            marrystatus
            sheng
            shi
    返回：

### 站内会员分类 图25 
    接口：/api/api/getlevellists
    参数：
            ---
    返回：
            [
                {
                    "id": 1,
                    "levelname": "A类会员",
                    "levelmoney": "19元/年",
                    "abstract": "1、送卷纸两提，马卡龙牙刷一打\n2、可以在小程序中按会员价购买所有的产品"
                },
                {
                    "id": 4,
                    "levelname": "黄金会员",
                    "levelmoney": "6666元/年",
                    "abstract": "可以选择并看到想认识的朋友详细资料，送“心动”点数60个，并可以根据客户要求由红娘推介12个精准有缘人并安排见面"
                },
                ……
            ]

### 取货地点列表
    接口：/api/api/getquhuoaddress
    参数：
            cityid     区域id
    返回：
            [
                {
                    "id": 1,
                    "title": "建东一取货点",
                    "sel_province": 130000000000,
                    "sel_city": 130400000000,
                    "sel_area": 130404000000,
                    "sel_qu": 130404004000,
                    "sel_jie": 130404004013,
                    "address": "dddddddd",
                    "longitudelatitude": "115.978579,39.490934",
                    "user": "aaaaaa",
                    "tel": "dddddd",
                    "status": 1,
                    "updatetime": 1598084254,
                    "addtime": 0
                },
                ……
            ]

### 根据区域id获取地址名称
    接口：/api/api/getcityname
    参数：
            id     区域id
    返回：
            区域名称

### 地址列表
    接口：/api/api/getcitys
    参数：
            pid     默认为 0
    返回：
            [
                {
                    "id":1,
                    "name":"直辖",
                    "pid":0
                    },
                {
                    "id":2,
                    "name":"河南省",
                    "pid":0
                }
                ……
            ]

### 从数据库返回系统配置信息   
    接口：/api/api/getconfig
    参数：
            --
    返回：
            {
                "shopname":"火烈鸟婚恋",
                "imgurl":"\/uploads\/images\/20200820\/3afaff97a89119231fbdf94f38139147.jpg",
                "tel":"0312-123456",
                "searchlevel": 4 // 会员高级搜索的限制级别
                "email":"ws_shop@ws_shop.com",
                "copyright":"copyright 2020 火烈鸟婚恋",
                "useragreements":"<p>身处在前端社区的繁荣之下，我们都在有意或无意地追逐。<\/p><p>而 layui 偏偏回望当初，奔赴在返璞归真的漫漫征途，自信并勇敢着，追寻于原生态的书写指令，试图以最简单的方式诠释高效。<\/p>",
                "policy":"<p>拥有双面的不仅是人生，还有 layui。一面极简，一面丰盈。<\/p><p>极简是视觉所见的外在，是开发所念的简易。丰盈是倾情雕琢的内在，是信手拈来的承诺。<\/p><p>一切本应如此，简而全，双重体验。<\/p>",
                "constellation":"白羊座,金牛座,双子座,巨蟹座,狮子座,室女座,天秤座,天蝎座,人马座,摩羯座,宝瓶座,双鱼座",
                "education":"初中,高中(中专),大学(大专),研究生,硕士,博士,博士后",
                
            }
